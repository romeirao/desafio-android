package ninja.resolve.githubjavapop.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Italo on 14/11/2015.
 */
@Parcel(Parcel.Serialization.BEAN)
public class PullRequest {
    @SerializedName("html_url")
    private String mHtmlUrl;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("body")
    private String mBody;
    @SerializedName("user")
    private UserData mOwner;

    public String getHtmlUrl() {
        return mHtmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.mHtmlUrl = htmlUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public UserData getOwner() {
        return mOwner;
    }

    public void setOwner(UserData mOwner) {
        this.mOwner = mOwner;
    }

    @Override
    public String toString() {
        return "PullRequest { title: " + mTitle + "; owner: " + mOwner.getUserNickname() + " }";
    }
}
