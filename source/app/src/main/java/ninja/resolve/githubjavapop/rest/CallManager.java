package ninja.resolve.githubjavapop.rest;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;

import retrofit.Call;

/**
 * Created by Italo on 18/11/2015.
 */
public class CallManager {
    private static final String TAG = CallManager.class.getSimpleName();

    private final Set<Call> calls = new HashSet<>();

    public void cancelAll(String tag) {
        if (calls.size() > 0) {
            Log.i(TAG, "stopping " + tag + " remain requests");
            for (final Call call : calls) {
                new Runnable() {
                    public void run() {
                        call.cancel();
                    }
                };
            }
            calls.clear();
        }
    }

    public void addCall(Call<?> call) {
        calls.add(call);
    }

    public void removeCall(Call<?> call) {
        calls.remove(call);
    }
}
