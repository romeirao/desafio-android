package ninja.resolve.githubjavapop.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import ninja.resolve.githubjavapop.R;
import ninja.resolve.githubjavapop.domain.Repository;
import ninja.resolve.githubjavapop.domain.UserData;
import ninja.resolve.githubjavapop.rest.CallManager;
import ninja.resolve.githubjavapop.rest.RestClient;
import ninja.resolve.githubjavapop.rest.services.GitHubService;
import retrofit.Call;

/**
 * Created by Italo on 14/11/2015.
 */
public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoryListAdapter.RepositoryListViewHolder> implements RestClient.OnRestListener<UserData> {
    private static final String TAG = RepositoryListAdapter.class.getSimpleName();

    private final CallManager mCallManager = new CallManager();
    private Context mContext;
    private OnListItemClickListener mListener;
    private final List<Repository> mRepositoryList;

    public RepositoryListAdapter(Context context, List<Repository> repositoryList, OnListItemClickListener listener) {
        mContext = context;
        mRepositoryList = repositoryList;
        mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mRepositoryList != null ? mRepositoryList.size() : 0;
    }

    @Override
    public RepositoryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cardview_item_main, parent, false);
        RepositoryListViewHolder holder = new RepositoryListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RepositoryListViewHolder holder, final int position) {
        Repository repository = mRepositoryList.get(position);

        holder.tvRepositoryName.setText(repository.getName());
        holder.tvRepositoryDescription.setText(repository.getDescription());
        holder.tvRepositoryAmountFork.setText(Integer.toString(repository.getAmountFork()));
        holder.tvRepositoryAmountStar.setText(Integer.toString(repository.getAmountStar()));

        UserData userData = repository.getOwner();
        holder.tvRepositoryUserNickname.setText(userData.getUserNickname());
        holder.tvRepositoryUserFullName.setText(userData.getUserFullName());
        Picasso.with(mContext).load(userData.getUserImageUrl()).placeholder(R.drawable.ic_user).into(holder.ivRepositoryUserImage);
        try {
            setUserName(RestClient.getInstance(mContext, GitHubService.BASE_URL), userData.getUserNickname(), holder.tvRepositoryUserFullName);
        } catch (IOException e) {
            Log.e(TAG, "There was a problem with the RestClient", e);
        }

        if (mListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListItemClick(holder.itemView, position);
                }
            });
        }
    }

    private void setUserName(RestClient<GitHubService> restClient, String ownerNickName, TextView userNameTextView) throws IOException {
        GitHubService gitHubService = restClient.getService(GitHubService.class);
        Call<UserData> call = gitHubService.getUserData(ownerNickName);
        mCallManager.addCall(call);
        restClient.getRestData(this, call, userNameTextView);
    }

    public void cancelCalls() {
        mCallManager.cancelAll(TAG);
    }

    @Override
    public void onIncomeResponse(UserData response, Call call, Object obj) {
        mCallManager.removeCall(call);
        TextView textView = (TextView) obj;
        textView.setText(response.getUserFullName());
    }

    @Override
    public void onIncomeFailure(Throwable t) {
        Log.e(TAG, "onFailure ", t);
    }

    public static class RepositoryListViewHolder extends RecyclerView.ViewHolder {
        public TextView tvRepositoryName;
        public TextView tvRepositoryDescription;
        public TextView tvRepositoryAmountFork;
        public TextView tvRepositoryAmountStar;
        public ImageView ivRepositoryUserImage;
        public TextView tvRepositoryUserNickname;
        public TextView tvRepositoryUserFullName;

        public RepositoryListViewHolder(View view) {
            super(view);

            tvRepositoryName = (TextView) view.findViewById(R.id.repositoryName);
            tvRepositoryDescription = (TextView) view.findViewById(R.id.repositoryDescription);
            tvRepositoryAmountFork = (TextView) view.findViewById(R.id.repositoryAmountFork);
            tvRepositoryAmountStar = (TextView) view.findViewById(R.id.repositoryAmountStar);
            ivRepositoryUserImage = (ImageView) view.findViewById(R.id.repositoryUserImage);
            tvRepositoryUserNickname = (TextView) view.findViewById(R.id.repositoryUserNickname);
            tvRepositoryUserFullName = (TextView) view.findViewById(R.id.repositoryUserFullName);
        }
    }

    public interface OnListItemClickListener {
        void onListItemClick(View view, int index);
    }
}
