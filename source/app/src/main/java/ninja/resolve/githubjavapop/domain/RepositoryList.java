package ninja.resolve.githubjavapop.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Italo on 15/11/2015.
 */
@Parcel(Parcel.Serialization.BEAN)
public class RepositoryList {
    @SerializedName("items")
    private List<Repository> mRepositoryList;

    public List<Repository> getRepositoryList() {
        return mRepositoryList;
    }

    public void setRepositoryList(List<Repository> repositoryList) {
        mRepositoryList = repositoryList;
    }
}
