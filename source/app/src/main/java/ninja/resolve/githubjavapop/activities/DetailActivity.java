package ninja.resolve.githubjavapop.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ninja.resolve.githubjavapop.R;
import ninja.resolve.githubjavapop.domain.Repository;
import ninja.resolve.githubjavapop.fragments.DetailFragment;

public class DetailActivity extends AppCompatActivity {
    public static final String DETAIL_DATA = "repository_data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Repository repository = (Repository) getIntent().getSerializableExtra(DETAIL_DATA);
        getSupportActionBar().setTitle(repository.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DetailFragment detailFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_detail);
        detailFragment.setRepository(repository);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
