package ninja.resolve.githubjavapop.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import ninja.resolve.githubjavapop.R;
import ninja.resolve.githubjavapop.activities.DetailActivity;
import ninja.resolve.githubjavapop.adapters.RepositoryListAdapter;
import ninja.resolve.githubjavapop.domain.Repository;
import ninja.resolve.githubjavapop.domain.RepositoryList;
import ninja.resolve.githubjavapop.listeners.EndlessScrollListener;
import ninja.resolve.githubjavapop.rest.CallManager;
import ninja.resolve.githubjavapop.rest.RestClient;
import ninja.resolve.githubjavapop.rest.services.GitHubService;
import ninja.resolve.githubjavapop.utils.NetworkUtils;
import ninja.resolve.githubjavapop.utils.ToastUtils;
import retrofit.Call;

/**
 * Created by Italo on 14/11/2015.
 */
public class MainFragment extends Fragment implements RestClient.OnRestListener<RepositoryList> {
    public static final String TAG = MainFragment.class.getSimpleName();

    private final CallManager mCallManager = new CallManager();
    private TextView mEmptyView;
    private ProgressBar mProgressBar;
    private ProgressBar mMiniProgressBar;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private List<Repository> mRepositoryList;
    private EndlessScrollListener mEndlessScrollListener;

    private int mCurrentPage = 1;
    private boolean mTaskIsRunning = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mEmptyView = (TextView) view.findViewById(R.id.empty_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mMiniProgressBar = (ProgressBar) view.findViewById(R.id.mini_progress);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.repositoryList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);

        mEndlessScrollListener = new EndlessScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                Log.d(TAG, "loading page: " + page);
                mCurrentPage = page;
                setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.VISIBLE);
                taskFetchRepositoryList();
            }
        };
        mRecyclerView.addOnScrollListener(mEndlessScrollListener);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mRepositoryList == null) {
            setVisibilityToContent(View.GONE, View.GONE, View.VISIBLE, View.GONE);
            taskFetchRepositoryList();
        } else {
            updateView();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh() {
        setVisibilityToContent(View.GONE, View.GONE, View.VISIBLE, View.GONE);

        if (mRepositoryList != null) {
            mCurrentPage = 1;
            mEndlessScrollListener.resetValues();
            int size = mRepositoryList.size();
            mRepositoryList.clear();
            mRecyclerView.getAdapter().notifyItemRangeRemoved(0, size);

        }

        taskFetchRepositoryList();
    }

    private void updateView() {
        if (mRepositoryList != null) {
            setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.GONE);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(new RepositoryListAdapter(getContext(), mRepositoryList, onListItemClick()));
        } else {
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private void setVisibilityToContent(int recyclerViewVisibility, int emptyViewVisibility, int progressBarVisibility, int miniProgressBarVisibility) {
        mRecyclerView.setVisibility(recyclerViewVisibility);
        mEmptyView.setVisibility(emptyViewVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mMiniProgressBar.setVisibility(miniProgressBarVisibility);
    }

    private synchronized void taskFetchRepositoryList() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            try {
                if (!mTaskIsRunning) {
                    Log.d(TAG, "task is running");
                    getRepositoryList();
                    mTaskIsRunning = true;
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                ToastUtils.alert(getActivity(), R.string.error_json);
                setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
            }
        } else {
            ToastUtils.alert(getActivity(), getResources().getString(R.string.error_no_connectivity));
            setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
        }
    }

    public void getRepositoryList() throws IOException {
        RestClient<GitHubService> restClient = RestClient.getInstance(getContext(), GitHubService.BASE_URL);
        GitHubService gitHubService = restClient.getService(GitHubService.class);
        Call<RepositoryList> call = gitHubService.listRepository(GitHubService.LANGUAGE_JAVA, GitHubService.SORT_STARS, mCurrentPage);
        mCallManager.addCall(call);
        restClient.getRestData(this, call);
    }

    private RepositoryListAdapter.OnListItemClickListener onListItemClick() {
        return new RepositoryListAdapter.OnListItemClickListener() {
            @Override
            public void onListItemClick(View view, int index) {
                Repository repository = mRepositoryList.get(index);
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.DETAIL_DATA, repository);
                startActivity(intent);
            }
        };
    }

    @Override
    public void onIncomeResponse(RepositoryList repositoryList, Call call, Object obj) {
        Log.d(TAG, "task is done");
        mCallManager.removeCall(call);
        if (repositoryList != null) {
            if (mRepositoryList != null) {
                mRepositoryList.addAll(repositoryList.getRepositoryList());
            } else {
                mRepositoryList = repositoryList.getRepositoryList();
            }
            Log.d(TAG, "mRepositoryList.size(): " + mRepositoryList.size());
            MainFragment.this.updateView();
        }
        mTaskIsRunning = false;
    }

    @Override
    public void onIncomeFailure(Throwable t) {
        Log.e(TAG, "onFailure ", t);
        if (mRepositoryList == null) {
            setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
        } else {
            setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.GONE);
        }
        mTaskIsRunning = false;
        ToastUtils.alert(getActivity(), t.getMessage());
    }

    @Override
    public void onDestroy() {
        mCallManager.cancelAll(TAG);
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
            ((RepositoryListAdapter) mRecyclerView.getAdapter()).cancelCalls();
        }
        super.onDestroy();
    }
}
