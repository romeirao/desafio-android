package ninja.resolve.githubjavapop.utils;

import android.app.Activity;
import android.widget.Toast;

/**
 * Created by Italo on 14/11/2015.
 */
public class ToastUtils {
    public static void alert(final Activity activity, final int idMsg) {
        alert(activity, activity.getString(idMsg));
    }

    public static void alert(final Activity activity, final String msg) {
        activity.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
                    }
                }
        );
    }
}
