package ninja.resolve.githubjavapop.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Italo on 14/11/2015.
 */
@Parcel(Parcel.Serialization.BEAN)
public class Repository implements Serializable {
    @SerializedName("name")
    private String mName;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("forks_count")
    private int mAmountFork;
    @SerializedName("stargazers_count")
    private int mAmountStar;
    @SerializedName("owner")
    private UserData mOwner;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public int getAmountFork() {
        return mAmountFork;
    }

    public void setAmountFork(int amountFork) {
        mAmountFork = amountFork;
    }

    public int getAmountStar() {
        return mAmountStar;
    }

    public void setAmountStar(int amountStar) {
        mAmountStar = amountStar;
    }

    public UserData getOwner() {
        return mOwner;
    }

    public void setOwner(UserData mOwner) {
        this.mOwner = mOwner;
    }

    @Override
    public String toString() {
        return "Repository { owner: " + mOwner.getUserFullName() + "; stars: " + mAmountStar + "; forks: " + mAmountFork + " }";
    }
}
