package ninja.resolve.githubjavapop.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import ninja.resolve.githubjavapop.R;
import ninja.resolve.githubjavapop.adapters.PullRequestListAdapter;
import ninja.resolve.githubjavapop.domain.PullRequest;
import ninja.resolve.githubjavapop.domain.Repository;
import ninja.resolve.githubjavapop.listeners.EndlessScrollListener;
import ninja.resolve.githubjavapop.rest.CallManager;
import ninja.resolve.githubjavapop.rest.RestClient;
import ninja.resolve.githubjavapop.rest.services.GitHubService;
import ninja.resolve.githubjavapop.utils.NetworkUtils;
import ninja.resolve.githubjavapop.utils.ToastUtils;
import retrofit.Call;


public class DetailFragment extends Fragment implements RestClient.OnRestListener<List<PullRequest>> {
    private static final String TAG = DetailFragment.class.getSimpleName();

    private final CallManager mCallManager = new CallManager();
    private Repository mRepository;
    private TextView mEmptyView;
    private ProgressBar mProgressBar;
    private ProgressBar mMiniProgressBar;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private List<PullRequest> mPullRequestList;
    private EndlessScrollListener mEndlessScrollListener;

    private int mCurrentPage = 1;
    private boolean mTaskIsRunning = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        mEmptyView = (TextView) view.findViewById(R.id.empty_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress);
        mMiniProgressBar = (ProgressBar) view.findViewById(R.id.mini_progress);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.pullRequestList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setHasFixedSize(true);

        mEndlessScrollListener = new EndlessScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                Log.d(TAG, "loading page: " + page);
                mCurrentPage = page;
                setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.VISIBLE);
                taskFetchPullRequestList();
            }
        };
        mRecyclerView.addOnScrollListener(mEndlessScrollListener);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPullRequestList == null) {
            setVisibilityToContent(View.GONE, View.GONE, View.VISIBLE, View.GONE);
            taskFetchPullRequestList();
        } else {
            updateView();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void refresh() {
        setVisibilityToContent(View.GONE, View.GONE, View.VISIBLE, View.GONE);

        if (mPullRequestList != null) {
            mCurrentPage = 1;
            mEndlessScrollListener.resetValues();
            int size = mPullRequestList.size();
            mPullRequestList.clear();
            mRecyclerView.getAdapter().notifyItemRangeRemoved(0, size);

        }

        taskFetchPullRequestList();
    }

    private void updateView() {
        if (mPullRequestList != null) {
            setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.GONE);
        }

        if (mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(new PullRequestListAdapter(getContext(), mPullRequestList, onListItemClick()));
        } else {
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    private void setVisibilityToContent(int recyclerViewVisibility, int emptyViewVisibility, int progressBarVisibility, int miniProgressBarVisibility) {
        mRecyclerView.setVisibility(recyclerViewVisibility);
        mEmptyView.setVisibility(emptyViewVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mMiniProgressBar.setVisibility(miniProgressBarVisibility);
    }

    private void taskFetchPullRequestList() {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            try {
                if (!mTaskIsRunning) {
                    Log.d(TAG, "task is running");
                    getPulRequestList();
                    mTaskIsRunning = true;
                }
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
                ToastUtils.alert(getActivity(), R.string.error_json);
                setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
            }
        } else {
            ToastUtils.alert(getActivity(), getResources().getString(R.string.error_no_connectivity));
            setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
        }
    }

    public void getPulRequestList() throws IOException {
        RestClient<GitHubService> restClient = RestClient.getInstance(getContext(), GitHubService.BASE_URL);
        GitHubService gitHubService = restClient.getService(GitHubService.class);
        Call<List<PullRequest>> call = gitHubService.listPullRequest(mRepository.getOwner().getUserNickname(), mRepository.getName(), mCurrentPage, GitHubService.TYPE_ALL);
        mCallManager.addCall(call);
        restClient.getRestData(this, call);
    }

    private PullRequestListAdapter.OnListItemClickListener onListItemClick() {
        return new PullRequestListAdapter.OnListItemClickListener() {
            @Override
            public void onListItemClick(View view, int index) {
                PullRequest pullRequest = mPullRequestList.get(index);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(pullRequest.getHtmlUrl()));
                startActivity(i);
            }
        };
    }

    @Override
    public void onIncomeResponse(List<PullRequest> pullRequestList, Call call, Object obj) {
        Log.d(TAG, "task is done");
        mCallManager.removeCall(call);
        if (pullRequestList != null) {
            if (mPullRequestList != null) {
                mPullRequestList.addAll(pullRequestList);
            } else {
                mPullRequestList = pullRequestList;
            }
            Log.d(TAG, "mPullRequestList.size(): " + mPullRequestList.size());
            DetailFragment.this.updateView();
        }
        mTaskIsRunning = false;
    }

    @Override
    public void onIncomeFailure(Throwable t) {
        Log.e(TAG, "onFailure", t);
        Log.e(TAG, "mCurrentPage: " + mCurrentPage);
        if (mPullRequestList == null) {
            setVisibilityToContent(View.GONE, View.VISIBLE, View.GONE, View.GONE);
        } else {
            setVisibilityToContent(View.VISIBLE, View.GONE, View.GONE, View.GONE);
        }
        mTaskIsRunning = false;
        ToastUtils.alert(getActivity(), t.getMessage());
    }

    @Override
    public void onDestroy() {
        mCallManager.cancelAll(TAG);
        if (mRecyclerView != null && mRecyclerView.getAdapter() != null) {
            ((PullRequestListAdapter) mRecyclerView.getAdapter()).cancelCalls();
        }
        super.onDestroy();
    }

    public void setRepository(Repository repository) {
        mRepository = repository;
    }
}
