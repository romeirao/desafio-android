package ninja.resolve.githubjavapop.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.io.Serializable;

/**
 * Created by Italo on 15/11/2015.
 */
@Parcel(Parcel.Serialization.BEAN)
public class UserData implements Serializable {
    @SerializedName("avatar_url")
    private String mUserImageUrl;
    @SerializedName("login")
    private String mUserNickname;
    @SerializedName("name")
    private String mUserFullName;

    public String getUserImageUrl() {
        return mUserImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        mUserImageUrl = userImageUrl;
    }

    public String getUserNickname() {
        return mUserNickname;
    }

    public void setUserNickname(String userNickname) {
        mUserNickname = userNickname;
    }

    public String getUserFullName() {
        return mUserFullName;
    }

    public void setUserFullName(String userFullName) {
        mUserFullName = userFullName;
    }

    @Override
    public String toString() {
        return "UserData { owner: " + mUserFullName + "; nickname: " + mUserNickname + " }";
    }
}
