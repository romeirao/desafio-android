package ninja.resolve.githubjavapop.rest;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Italo on 18/11/2015.
 */
public class RestClient<RestService> {
    private static final String TAG = RestClient.class.getSimpleName();

    private static final int MAX_STALE = 60; // 1 minute
    private static final int MAX_AGE = 60 * 60; // 1 hour

    private static RestClient instance;

    private Retrofit retrofit;

    public synchronized static RestClient getInstance(Context context, String url) {
        if (instance == null) {
            instance = new RestClient(context, url);
        }
        return instance;
    }

    private RestClient(Context context, String url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(10, TimeUnit.SECONDS);
        client.setWriteTimeout(10, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);
        client.interceptors().add(new CacheInterceptor());

        try {
            long cacheSize = 10 * 1024 * 1024; // 10 MiB
            File httpCacheDirectory = new File(context.getCacheDir(), "responses");

            Cache cache = new Cache(httpCacheDirectory, cacheSize);
            Log.d(TAG, "Got this one: " + cache.getDirectory().getAbsolutePath());
            client.setCache(cache);
        } catch (Exception e) {
            Log.d(TAG, "Unable to set http cache", e);
        }

        retrofit = new Retrofit.Builder().client(client).baseUrl(url).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    public <T> void getRestData(final OnRestListener listener, final Call<T> call) throws IOException {
        getRestData(listener, call, null);
    }

    public <T> void getRestData(final OnRestListener listener, final Call<T> call, final Object obj) throws IOException {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Response<T> response, Retrofit retrofit) {
                if (response.isSuccess() && response.code() == 200) {
                    listener.onIncomeResponse(response.body(), call, obj);
                } else if (response.code() == 403) {
                    onFailure(new Exception("CODE RECEIVED: " + response.code() + " - You've reached the maximum limit access."));
                } else {
                    onFailure(new Exception("CODE RECEIVED: " + response.code() + " - You got a problem with this access."));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onIncomeFailure(t);
            }
        });
    }

    public RestService getService(Class<RestService> service) {
        return retrofit.create(service);
    }

    static class CacheInterceptor implements Interceptor {
        @Override
        public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Cache-Control", String.format("max-age=%d, max-stale=%d", MAX_AGE, MAX_STALE))
                    .method(original.method(), original.body())
                    .build();

            long t1 = System.nanoTime();
            //Log.d(TAG, String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));
            Log.d(TAG, String.format("Sending request %s on %s", original.url(), chain.connection()));

            // Request
            com.squareup.okhttp.Response response = chain.proceed(request);

            if (response.cacheResponse() != null) {
                Log.i(TAG, "response comes from cache. url: " + request.url());
                Log.i(TAG, response.cacheResponse().toString());
            } else if (response.networkResponse() != null) {
                Log.i(TAG, "response comes from network. url: " + request.url());
                Log.i(TAG, response.networkResponse().toString());
            } else {
                Log.e(TAG, "no response from cache neither from network. url: " + request.url());
                Log.e(TAG, request.toString());
            }

            long t2 = System.nanoTime();
            //Log.d(TAG, String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));
            Log.d(TAG, String.format("Received response for %s in %.1fms", response.request().url(), (t2 - t1) / 1e6d));

            return response;
        }
    }

    public interface OnRestListener<T> {
        void onIncomeResponse(T response, Call call, Object obj);

        void onIncomeFailure(Throwable t);
    }
}
