package ninja.resolve.githubjavapop.rest.services;

import java.util.List;

import ninja.resolve.githubjavapop.domain.PullRequest;
import ninja.resolve.githubjavapop.domain.RepositoryList;
import ninja.resolve.githubjavapop.domain.UserData;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Italo on 15/11/2015.
 */
public interface GitHubService {
    String BASE_URL = "https://api.github.com";
    String LANGUAGE_JAVA = "language:Java";
    String SORT_STARS = "stars";
    String TYPE_ALL = "all";

    @GET("/search/repositories")
    Call<RepositoryList> listRepository(@Query("q") String query, @Query("sort") String sort, @Query("page") int page);

    @GET("/repos/{owner}/{repository}/pulls")
    Call<List<PullRequest>> listPullRequest(@Path("owner") String owner, @Path("repository") String repositoryName, @Query("page") int page, @Query("state") String state);

    @GET("/users/{owner}")
    Call<UserData> getUserData(@Path("owner") String owner);
}
